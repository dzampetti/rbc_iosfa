﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RBC_IOSFA.Models;


namespace RBC_IOSFA.Gestores
{
    public class GestorNomenclador
    {
        #region "PATRON SINGLETON"
        private static GestorNomenclador nomenclador = null;
        private GestorNomenclador() { }
        public static GestorNomenclador GetInstance()
        {
            if (nomenclador == null)
            {
                nomenclador = new GestorNomenclador();
            }
            return nomenclador;
        }
        #endregion

        public List<Nomenclador> ConsultarNomenclador()
        {
            List<Nomenclador> Nomenclador = new List<Nomenclador>();

            using (var db = new Models.RBC_IOSFA_Entities())
            {
                Nomenclador = db.Nomenclador.ToList();
            }
            return (Nomenclador);
        }
    }
}