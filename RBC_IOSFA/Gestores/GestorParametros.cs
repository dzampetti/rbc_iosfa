﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RBC_IOSFA.Models;

namespace RBC_IOSFA.Gestores
{
    public class GestorParametros
    {
        #region "PATRON SINGLETON"
        private static GestorParametros parametros = null;
        private GestorParametros() { }
        public static GestorParametros GetInstance()
        {
            if (parametros == null)
            {
                parametros = new GestorParametros();
            }
            return parametros;
        }
        #endregion

        public List<Parametros> ConsultarParametros()
        {
            List<Parametros> Parametros = new List<Parametros>();

            using (var db = new Models.RBC_IOSFA_Entities())
            {
                Parametros = db.Parametros.ToList();                
            }
            return (Parametros);
        }

    }
}