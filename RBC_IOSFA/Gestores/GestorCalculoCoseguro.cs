﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RBC_IOSFA.Models;
using System.IO;
using System.Globalization;

namespace RBC_IOSFA.Gestores
{
    public class GestorCalculoCoseguro
    {
        #region "PATRON SINGLETON"
        private static GestorCalculoCoseguro nomenclador = null;
        private GestorCalculoCoseguro() { }
        public static GestorCalculoCoseguro GetInstance()
        {
            if (nomenclador == null)
            {
                nomenclador = new GestorCalculoCoseguro();
            }
            return nomenclador;
        }
        #endregion

        public PedidoCalculoCoseguro PedidoCalculoCoseguro(PedidoCalculoCoseguro pedido )
        {
            try
            {
                if (Directory.Exists("E:/logs_biosoft/IOSFA/" + DateTime.Today.Year.ToString()) == false)
                    Directory.CreateDirectory("E:/logs_biosoft/IOSFA/" + DateTime.Today.Year.ToString());

                StreamWriter sw = new StreamWriter(@"E:/logs_biosoft/IOSFA/" + DateTime.Today.Year.ToString() + "/" + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(DateTime.Today.Month) + ".txt", true);
                sw.WriteLine(DateTime.Today.ToString());
                sw.WriteLine("Se agrega prestacion: " + pedido.CodPracticaAgrega.ToString());
                sw.WriteLine("************************************************************************************************");
                sw.Close();
            }
            catch (Exception)
            {
                pedido.EstadoTransaccion = "Error al grabar el log";
                return pedido;
            }

            return pedido;
        }



        public RespuestaCalculoCoseguro CalculoCoseguro(RespuestaCalculoCoseguro listaActualizada)
        {
            


            
            return listaActualizada;
        }
        




    }

}