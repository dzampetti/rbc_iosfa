﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RBC_IOSFA.Models;

namespace RBC_IOSFA.Gestores
{
    public class GestorTipoPrestacion
    {
        #region "PATRON SINGLETON"
        private static GestorTipoPrestacion tipo = null;
        private GestorTipoPrestacion() { }
        public static GestorTipoPrestacion GetInstance()
        {
            if (tipo == null)
            {
                tipo = new GestorTipoPrestacion();
            }
            return tipo;
        }
        #endregion

        public TipoPrestacion ConsultarTipoPrestacion(string id) //este metodo esta para analizar pq no funciona
        {
            
            TipoPrestacion oDescripcion = new Models.TipoPrestacion();
            using (var db = new RBC_IOSFA_Entities())
            {
                oDescripcion.idTipoPrestacion = db.TipoPrestacion.Where(a => a.idTipoPrestacion == id).ToString();
                oDescripcion.descripcionTipoPrestacion = db.TipoPrestacion.Where(a => a.descripcionTipoPrestacion == id).ToString();
            }
            return (oDescripcion);
        }


        public string ConsultarDescripcionPrestacion(string id) //este metodo esta para analizar pq no funciona
        {

            TipoPrestacion oDescripcion = new Models.TipoPrestacion();
            using (var db = new RBC_IOSFA_Entities())
            {
                oDescripcion.idTipoPrestacion = db.TipoPrestacion.Where(a => a.idTipoPrestacion == id).ToString();
                oDescripcion.descripcionTipoPrestacion = db.TipoPrestacion.Where(a => a.descripcionTipoPrestacion == id).ToString();
            }
            return (oDescripcion.descripcionTipoPrestacion);
        }

        public List<TipoPrestacion> ConsultarTiposPrestacion()
        {
            List<TipoPrestacion> Tipos = new List<TipoPrestacion>();

            using (var db = new RBC_IOSFA_Entities())
            {
               Tipos = db.TipoPrestacion.ToList();
            }

            return Tipos;
        }
    }
}