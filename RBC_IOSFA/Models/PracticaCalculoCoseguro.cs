﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RBC_IOSFA.Models
{
    public class PracticaCalculoCoseguro
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Posicion { get; set; }
        public int Cantidad { get; set; }
        public string TipoCoseguro { get; set; }
    }
}