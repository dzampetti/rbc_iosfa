﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RBC_IOSFA.Models
{
    public class RespuestaCalculoCoseguro
    {
        public string DescripcionDetBioqBasica { get; set; }
        public string ImporteDetBioqBasica { get; set; }
        public string DescripcionDetBioqBajaComplejidad { get; set; }
        public string ImporteDetBioqBajaComplejidad { get; set; }
        public string DescripcionDetBioqMedianaComplejidad { get; set; }
        public string ImporteDetBioqMedianaComplejidad { get; set; }
        public string DescripcionDetBioqAltaComplejidad { get; set; }
        public string ImporteDetBioqAltaComplejidad { get; set; }
        public string DescripcionDetBioqPorPresupuesto { get; set; }
        public string ImporteDetBioqPorPresupuesto { get; set; }
        public string ImporteTotal { get; set; }
        public List<PracticaCalculoCoseguro> Practicas { get; set; }
    }
}