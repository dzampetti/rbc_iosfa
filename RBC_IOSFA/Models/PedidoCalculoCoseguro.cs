﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RBC_IOSFA.Models
{
    public class PedidoCalculoCoseguro
    {
        public int CodPracticaAgrega { get; set; }
        public List<PracticaCalculoCoseguro> Practicas { get; set; }
        public string EstadoTransaccion { get; set; }
    }
}