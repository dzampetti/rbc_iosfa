﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RBC_IOSFA.Gestores;
using RBC_IOSFA.Models;

namespace RBC_IOSFA.Controllers
{

    [AllowAnonymous]
    [RoutePrefix("api/coseguros")]
    public class PedidoCalculoCoseguroController : ApiController
            
    {
        private RBC_IOSFA_Entities db = new RBC_IOSFA_Entities();

        [HttpGet]
        [Route("PedidoCalculoCoseguro")]
        public IHttpActionResult PedidoCalculoCoseguro(int CodPractica, GestorCalculoCoseguro pedido )
                 
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

               

                return Ok(content: "Grabo log OK " + CodPractica.ToString());
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }
    }

}
