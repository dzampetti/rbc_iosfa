﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RBC_IOSFA.Gestores;
using RBC_IOSFA.Models;

namespace RBC_IOSFA.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/coseguros")]
    public class TipoPrestacionController : ApiController
    {
        [HttpGet]
        [Route("ConsultarTipoPrestacion")]
        public IHttpActionResult ConsultarTipoPrestacion(string id)
        {
            try
            {
                return Ok(content: GestorTipoPrestacion.GetInstance().ConsultarTipoPrestacion(id));
            }   
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }

        [HttpGet]
        [Route("ConsultarDescripcionPrestacion")]
        public IHttpActionResult ConsultarDescripcionPrestacion(string id)
        {
            try
            {
                return Ok(content: GestorTipoPrestacion.GetInstance().ConsultarDescripcionPrestacion(id));
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }

        [HttpGet]
        [Route("ConsultarTiposPrestacion")]
        public IHttpActionResult ConsultarTiposPrestacion()
        {
            try
            {
                return Ok(content: GestorTipoPrestacion.GetInstance().ConsultarTiposPrestacion());
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }
    }
}
