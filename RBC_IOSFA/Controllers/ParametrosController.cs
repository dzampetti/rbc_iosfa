﻿using RBC_IOSFA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RBC_IOSFA.Gestores;

namespace RBC_IOSFA.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/coseguros")]
    public class ParametrosController : ApiController
    {
        [HttpGet]
        [Route("ConsultarParametros")]
        public IHttpActionResult ConsultarParametros()
        {
            try
            {
                return Ok(content: GestorParametros.GetInstance().ConsultarParametros());
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.InternalServerError, "Error indefinido en la API");
            }
        }
    }
}
