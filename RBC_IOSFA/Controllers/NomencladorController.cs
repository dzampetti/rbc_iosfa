﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RBC_IOSFA.Models;

namespace RBC_IOSFA.Controllers
{
    public class NomencladorController : ApiController
    {
        private RBC_IOSFA_Entities db = new RBC_IOSFA_Entities();

        // GET: api/Nomenclador
        public IQueryable<Nomenclador> GetNomenclador()
        {
            return db.Nomenclador;
        }

        // GET: api/Nomenclador/5
        [ResponseType(typeof(Nomenclador))]
        public async Task<IHttpActionResult> GetNomenclador(int id)
        {
            Nomenclador nomenclador = await db.Nomenclador.FindAsync(id);
            if (nomenclador == null)
            {
                return NotFound();
            }

            return Ok(nomenclador);
        }

        // PUT: api/Nomenclador/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutNomenclador(int id, Nomenclador nomenclador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nomenclador.idPrestacion)
            {
                return BadRequest();
            }

            db.Entry(nomenclador).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NomencladorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Nomenclador
        [ResponseType(typeof(Nomenclador))]
        public async Task<IHttpActionResult> PostNomenclador(Nomenclador nomenclador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Nomenclador.Add(nomenclador);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = nomenclador.idPrestacion }, nomenclador);
        }

        // DELETE: api/Nomenclador/5
        [ResponseType(typeof(Nomenclador))]
        public async Task<IHttpActionResult> DeleteNomenclador(int id)
        {
            Nomenclador nomenclador = await db.Nomenclador.FindAsync(id);
            if (nomenclador == null)
            {
                return NotFound();
            }

            db.Nomenclador.Remove(nomenclador);
            await db.SaveChangesAsync();

            return Ok(nomenclador);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NomencladorExists(int id)
        {
            return db.Nomenclador.Count(e => e.idPrestacion == id) > 0;
        }
    }
}